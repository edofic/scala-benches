import akka.actor._
import concurrent.{Await,Promise}
import concurrent.duration._

object Main {
  def main(args: Array[String]){
    val sys = ActorSystem()
    sys.actorOf(Props(new SimpleRunner(args(0).toInt, args(1).toInt)))
    readLine()
    sys.shutdown()
  }
}

class SimpleRunner(k: Int, n: Int) extends Actor {
  var i: Int = 0
  var start: Long = _
  var pinger: ActorRef = _
  
  init()
  
  def receive = {
    case _ =>
      val took = System.currentTimeMillis - start
      println(f"took $took ms, ${n.toFloat/took} per ms")
      init()
  }
  
  def init() {
    if(i>=k){
      context.system.shutdown()
      self ! PoisonPill
      return
    }
    i += 1 
    if(pinger ne null) pinger ! PoisonPill
    start = System.currentTimeMillis
    pinger = context.actorOf(Props(new SimplePinger(n)))
  }
}

class SimplePinger(n: Int) extends Actor {
  var count = n-1
  val ponger = context.actorOf(Props[SimplePonger])
  
  {
    var i = 0
    while(i<n){
      ponger ! () 
      i += 1
    }
  }
  
  def receive = {
    case _ => 
      count -=1
      if (count <= 0) {
        context.actorFor("..") ! ()
        context.become(PartialFunction.empty)
      }
  }
}

class SimplePonger extends Actor {
  def receive = {
    case msg => 
      sender ! msg
  } 
}
